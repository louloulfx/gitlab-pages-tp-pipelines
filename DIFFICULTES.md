# Difficultés rencontrées

Nous avons eu du mal à lancer nos tests, on a donc dû utiliser container registry dans notre stage de build afin de pouvoir réutiliser nos containers dans nos jobs de tests afin de faire passer nos tests dans nos containers déjà monter. Cela nous a permis de gagner du temps de pipeline et aussi d’exécuter nos tests dans un environnement semblable à la production.

Nous avons eu du mal à exécuter nos tests aussi car ils devaient utiliser la BDD alors que normalement les tests unitaires doivent être indépendants. Nous avons donc refaits nos tests unitaires afin de les voir passer dans notre pipeline.

Nous avons rencontré des problèmes avec le déploiement sur une plateforme, nous avons essayer de déployer sur heroku, mais avec notre manque de temps et de compétences sur ce domaine nous avons préféré finir ce qui était demandé.

Nous avons eu un peu de mal à prendre en main gitlab-ci car nous n'étions pas familier avec les pipelines en général. Elouan avait quelques notions, et Corentin découvrait complètement cet univers.
