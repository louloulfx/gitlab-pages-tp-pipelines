# Summary

- [Tests](README.md)
- [Difficultés rencontrées](DIFFICULTES.md)
- [Déploiement](DEPLOY.md)
- [Paradigmes](PARADIGME.md)
