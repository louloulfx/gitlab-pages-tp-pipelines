# Paradigmes exploités

## Devops

Le domaine DevOps regroupe comme son nom l’indique le développement et l’opération (exploitation). Le devops permet à une idée (correction de bugs, nouvelle fonctionnalité …) de passer de la phase de développement à celle de déploiement grâce à des approches d’accélération de processus.

## CI - Continuous Integration

CI – Continuous Integration correspond à l’intégration continue. Elle correspond à un ensemble de pratiques permettant de faire des tests automatiquement à chaque mise à jour du code avant de le déployer en production.

## CD – Continuous Delivery

CD – Continuous Delivery correspond à la livraison continue. Elle correspond à la mise à disposition d’un produit prêt à être déployé automatiquement.

## CD – Continuous Deployment

CD – Continuous Deployment correspond au développement continu. Elle correspond à un ensemble de pratiques permettant de déployer automatiquement un projet à chaque mise à jour du code. Le déploiement continu se déroule à la suite de l’intégration continue.

## Gitlab-ci

### Job

Les jobs sont l'élément le plus important d'un fichier gitlab-ci.yml car ils permettent d’exécuter les différents scripts qu'on lui fournis, comme par exemple exécuter des tests. Ils sont défini avec des contraintes indiquant dans quelles conditions ils doivent être exécutés. Ce sont des éléments de niveau supérieur avec un nom arbitraire et ils doivent contenir au moins la clause de script. Ils ne sont pas limité dans le nombre.

### Script

Le script permet d'indiquer les actions à effectuer.

### Image

Une image Docker est une représentation statique de l'application ou du service, de leur configuration et de leurs dépendances. Les images peuvent être utilisées dans les jobs.

### Container

Un container contient un ensemble de processus regroupant les fichiers nécessaire à son exécution.

### When

Le when dans gitlab-ci permet au job de savoir quand est-ce qu'il doit être exécuté.

### Allow_failure

Le allow_failure dans gitlab-ci permet de dire au job si il peut passer la main au job suivant si il fail.

### Environnement

Le environnement dans gitlab-ci permet de créer un environnement lors d'un déploiement. On peut lui donner un nom, une url, une condition, ou une action.

### Stage

Le stage permet de regrouper plusieurs job a exécuté en même temps (ex: deploy, test, build ... ).

### Directives

Les directives permettent de savoir sur quels branche par exemple un job doit être exécuté.

### Cache

Un cache permet simplement de spécifier les fichiers et répertoires à mettre en cache durant la pipeline.

### Artifacts

Les artifacts permettent de réutiliser des containers par exemple dans plusieurs pipelines.
