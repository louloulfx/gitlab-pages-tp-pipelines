# Tests

## Tests unitaires

Nous avons faits des tests unitaires sur le back. Nous avons utilisé Jest afin de faire nos tests.

Dans un premier temps nous avons fait un test sur l'inscription :

```js
it("Post signup", (done) => {
  const expectedResponse = {};
  request(app)
    .post("/signup")
    .send(userBody)
    .expect(200)
    .end((err, res) => {
      expect(res.body).toEqual(expectedResponse);
      done();
    });
});
```

Ce test nous permet de tester que lorsque nous faisons un post sur signup avec un user valide (userBody) notre endpoint doit bien nous renvoyer un objet vide.

## Tests de sécurités

Pour nos tests de sécurités nous avons utilisé [Trivy](https://github.com/aquasecurity/trivy) . Cet outil nous permet de faire des tests de sécurités sur nos images front et back afin de voir si nos images comportent des vulnérabilités.
