# Deploiement

## Notre choix

Nous avons décidé de partir sur un déploiement continu. C'est à dire qu'à chaque fois que nous faisons une modification sur la branche develop nous faisons un déploiement sur un environnement de recette (staging). Sur la branche develop on fais passer tous les jobs de gitlab-ci :

![Pipelines](Pipelines.PNG)

Donc nous faisons passer les build du back et du front, les tests unitaires, de qualité et de sécurités ainsi que le déploiement en recette.

Et pour la branche master nous passons uniquement le job de déploiement en prod et cette branche est aussi en déploiement continu.
